Repository for submitted solutions for HackerHank programming challenges. Each problem is implemented as a single class and the description and test cases can be found in the links provided at their headers. The links can also be found in my profile:

https://www.hackerrank.com/mandriaza