package search;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @see <a href="https://www.hackerrank.com/challenges/hackerland-radio-transmitters">Hackerland Radio Transmitters</a>
 */
public class HackerlandRadioTransmitters {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int k = in.nextInt();
		int[] x = new int[n];
		for (int i = 0; i < n; i++) {
			x[i] = in.nextInt();
		}
		in.close();
		//
		Arrays.sort(x);
		int pivot = 0;
		int i = 1;
		boolean isBorder = true;
		int transmitterCount = 0;
		do {
			while (i < x.length && x[i] - x[pivot] <= k) {
				i++;
			}
			if (isBorder) {
				isBorder = false;
				pivot = i - 1;
				transmitterCount++;
			} else {
				isBorder = true;
				pivot = i;
			}
		} while (i < x.length);
		System.out.println(transmitterCount);
	}
}