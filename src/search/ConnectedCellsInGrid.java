package search;

import java.util.Scanner;

/**
 * @see <a href=
 *      "https://www.hackerrank.com/challenges/connected-cell-in-a-grid">Connected
 *      Cells in a Grid</a>
 */
public class ConnectedCellsInGrid {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int m = in.nextInt();
		int[][] M = new int[n][m];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				M[i][j] = in.nextInt();
			}
		}
		in.close();
		//
		int maxRegion = Integer.MIN_VALUE;
		int nextRegionId = 2;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if (M[i][j] == 1) {
					int neighbords = findAndMarkNeighbords(M, n, m, i, j, nextRegionId++);
					if (neighbords > maxRegion) {
						maxRegion = neighbords;
					}
				}
			}
		}
		System.out.println(maxRegion);
	}

	private static int findAndMarkNeighbords(int[][] M, int n, int m, int i, int j, int regionId) {
		int count = 1;
		M[i][j] = regionId;
		if (j + 1 < m) {
			if (i - 1 >= 0 && M[i - 1][j + 1] == 1) {
				count += findAndMarkNeighbords(M, n, m, i - 1, j + 1, regionId);
			}
			if (M[i][j + 1] == 1) {
				count += findAndMarkNeighbords(M, n, m, i, j + 1, regionId);
			}
			if (i + 1 < n && M[i + 1][j + 1] == 1) {
				count += findAndMarkNeighbords(M, n, m, i + 1, j + 1, regionId);
			}
		}
		if (i + 1 < n && M[i + 1][j] == 1) {
			count += findAndMarkNeighbords(M, n, m, i + 1, j, regionId);
		}
		if (j - 1 >= 0) {
			if (i + 1 < n && M[i + 1][j - 1] == 1) {
				count += findAndMarkNeighbords(M, n, m, i + 1, j - 1, regionId);
			}
			if (M[i][j - 1] == 1) {
				count += findAndMarkNeighbords(M, n, m, i, j - 1, regionId);
			}
			if (i - 1 >= 0 && M[i - 1][j - 1] == 1) {
				count += findAndMarkNeighbords(M, n, m, i - 1, j - 1, regionId);
			}
		}
		if (i - 1 >= 0 && M[i - 1][j] == 1) {
			count += findAndMarkNeighbords(M, n, m, i - 1, j, regionId);
		}
		return count;
	}
}
