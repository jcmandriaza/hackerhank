package implementation;

import java.util.Scanner;

/**
 * @see <a href="https://www.hackerrank.com/challenges/bomber-man">The Bomberman Game</a>
 */
public class TheBombermanGame {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int R = in.nextInt();
		int C = in.nextInt();
		int N = in.nextInt();
		int[][] grid = new int[R][C];
		for (int i = 0; i < R; i++) {
			char[] row = in.next().toCharArray();
			for (int j = 0; j < C; j++)
				if (N == 1)
					grid[i][j] = (row[j] == '.' ? 8 : 0);
				else
					grid[i][j] = (row[j] == '.' ? 4 : 1);
		}
		in.close();
		int EXPLODED_AREA = 8;
		int COUNTING_BOMB = 7;
		System.out.println(System.currentTimeMillis());
		boolean willExplode = true;
		int count = (N - 3) % 4;
		for (int i = 0; i <= count; i++) {
			for (int j = 0; j < R; j++) {
				for (int k = 0; k < C; k++) {
					grid[j][k] >>= 1;
				}
			}
			if (willExplode) {
				// First row
				if ((grid[0][0] & COUNTING_BOMB) == 0) {
					grid[0][0] = EXPLODED_AREA;
					if (C > 1) {
						grid[0][1] |= EXPLODED_AREA;
					}
					if (R > 1) {
						grid[1][0] |= EXPLODED_AREA;
					}
				}
				if (C > 1) {
					for (int k = 1; k < C - 1; k++) {
						if ((grid[0][k] & COUNTING_BOMB) == 0) {
							grid[0][k] = EXPLODED_AREA;
							//
							grid[0][k - 1] |= EXPLODED_AREA;
							grid[0][k + 1] |= EXPLODED_AREA;
							if (R > 1) {
								grid[1][k] |= EXPLODED_AREA;
							}
						}
					}
					if ((grid[0][C - 1] & COUNTING_BOMB) == 0) {
						grid[0][C - 1] = EXPLODED_AREA;
						grid[0][C - 2] |= EXPLODED_AREA;
						if (R > 1) {
							grid[1][C - 1] |= EXPLODED_AREA;
						}
					}
				}
				if (R > 1) {
					// Intermediate rows
					for (int j = 1; j < R - 1; j++) {
						if ((grid[j][0] & COUNTING_BOMB) == 0) {
							grid[j][0] = EXPLODED_AREA;
							grid[j - 1][0] |= EXPLODED_AREA;
							if (C > 1) {
								grid[j][1] |= EXPLODED_AREA;
							}
							grid[j + 1][0] |= EXPLODED_AREA;
						}
						for (int k = 1; k < C - 1; k++) {
							if ((grid[j][k] & COUNTING_BOMB) == 0) {
								grid[j][k] = EXPLODED_AREA;
								//
								grid[j - 1][k] |= EXPLODED_AREA;
								grid[j + 1][k] |= EXPLODED_AREA;
								grid[j][k - 1] |= EXPLODED_AREA;
								grid[j][k + 1] |= EXPLODED_AREA;
							}
						}
						if ((grid[j][C - 1] & COUNTING_BOMB) == 0) {
							grid[j][C - 1] = EXPLODED_AREA;
							grid[j - 1][C - 1] |= EXPLODED_AREA;
							if (C > 1) {
								grid[j][C - 2] |= EXPLODED_AREA;
							}
							grid[j + 1][C - 1] |= EXPLODED_AREA;
						}
					}
					// Last row
					if ((grid[R - 1][0] & COUNTING_BOMB) == 0) {
						grid[R - 1][0] = EXPLODED_AREA;
						if (C > 1) {
							grid[R - 1][1] |= EXPLODED_AREA;
						}
						grid[R - 2][0] |= EXPLODED_AREA;
					}
					for (int k = 1; k < C - 1; k++) {
						if ((grid[R - 1][k] & COUNTING_BOMB) == 0) {
							grid[R - 1][k] = EXPLODED_AREA;
							//
							grid[R - 1][k - 1] |= EXPLODED_AREA;
							grid[R - 1][k + 1] |= EXPLODED_AREA;
							grid[R - 2][k] |= EXPLODED_AREA;
						}
					}
					if ((grid[R - 1][C - 1] & COUNTING_BOMB) == 0) {
						grid[R - 1][C - 1] = EXPLODED_AREA;
						if (C > 1) {
							grid[R - 1][C - 2] |= EXPLODED_AREA;
						}
						grid[R - 2][C - 1] |= EXPLODED_AREA;
					}
				}
			}
			willExplode = !willExplode;
		}
		System.out.println(System.currentTimeMillis());
		for (int j = 0; j < R; j++) {
			StringBuffer buffer = new StringBuffer();
			for (int k = 0; k < C; k++)
				buffer.append((grid[j][k] & EXPLODED_AREA) != 0 ? '.' : 'O');
			System.out.println(buffer);
		}
	}
}
