package dynamicprogramming;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @see <a href="https://www.hackerrank.com/challenges/decibinary-numbers">Decibinary Numbers</a>
 */
public class DecibinaryNumbers {

	public static void main(String[] args) {
		Map<Long, Long> table = new HashMap<>();
		table.put(0L, 1L);
		table.put(1L, 1L);
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		for (int i = 0; i < N; i++) {
			long x = in.nextLong();
			long num = 0, count = 1;
			while (count < x) {
				count += getNumberOfRepresentations(++num, table);
			}
			StringBuffer representation = new StringBuffer();
			findRepresentationAt(getLargestDecibinaryFor(num), 0, count - x, representation);
			System.out.println(representation);
		}
		in.close();
	}

	private static long findRepresentationAt(int[] decibinaryValue, int start, long position, StringBuffer buffer) {
		if (!canRepresent(decibinaryValue, start)) {
			return position;
		} else if (position == 0) {
			boolean isSignificant = (decibinaryValue.length == 1 ? true : false);
			int remainer = 0;
			for (int digit : decibinaryValue) {
				digit += remainer * 2;
				if (digit > 9) {
					remainer = digit - 9;
					digit = 9;
				} else {
					remainer = 0;
				}
				if (digit == 0 && !isSignificant) {
					continue;
				} else {
					if (digit != 0) {
						isSignificant = true;
					}
					buffer.append(digit);
				}
			}
			return position;
		} else if (start + 1 == decibinaryValue.length) {
//			System.out.println(Arrays.toString(decibinaryValue));
			return position - 1;
		} else {
			long lastPosition = Long.MIN_VALUE;
			int[] decibinaryCopy = Arrays.copyOf(adjustRepresentation(decibinaryValue, start), decibinaryValue.length);
			while (decibinaryCopy[start] >= 0 && lastPosition != position) {
				lastPosition = position;
				position = findRepresentationAt(decibinaryCopy, start + 1, position, buffer);
				if (buffer.length() != 0) {
					break;
				}
				decibinaryCopy[start]--;
				decibinaryCopy[start + 1] += 2;
			}
			return position;
		}
	}

	private static int[] getLargestDecibinaryFor(long num) {
		char[] binaryRepresentation = Long.toBinaryString(num).toCharArray();
		int[] decibinaryRepresentation = new int[binaryRepresentation.length];
		for (int i = 0; i < binaryRepresentation.length; i++) {
			decibinaryRepresentation[i] = binaryRepresentation[i] - '0';
		}
		return decibinaryRepresentation;
	}

	private static long getNumberOfRepresentations(long num, Map<Long, Long> table) {
		if (table.containsKey(num)) {
			return table.get(num);
		} else {
			long total = calculateRepresentations(getLargestDecibinaryFor(num), 0);
			table.put(num, total);
			table.put(num + 1, total);
			return total;
		}
	}

	private static long calculateRepresentations(int[] decibinaryValue, int start) {
		if (!canRepresent(decibinaryValue, start)) {
			return 0;
		} else if (start + 1 == decibinaryValue.length) {
			return 1;
		} else {
			long lastCount = Long.MIN_VALUE, count = 0;
			int[] decibinaryCopy = Arrays.copyOf(adjustRepresentation(decibinaryValue, start), decibinaryValue.length);
			while (decibinaryCopy[start] >= 0 && lastCount != count) {
				lastCount = count;
				count += calculateRepresentations(decibinaryCopy, start + 1);
				decibinaryCopy[start]--;
				decibinaryCopy[start + 1] += 2;
			}
			return count;
		}
	}

	private static int[] adjustRepresentation(int[] decibinaryValue, int start) {
		if (decibinaryValue[start] > 9) {
			decibinaryValue[start + 1] += (decibinaryValue[start] - 9) * 2;
			decibinaryValue[start] = 9;
		}
		return decibinaryValue;
	}

	private static boolean canRepresent(int[] decibinaryValue, int start) {
		long maxPosible = 0, currentValue = 0;
		for (long power = decibinaryValue.length - start - 1; start < decibinaryValue.length; start++, power--) {
			maxPosible += 9 * (1 << power);
			currentValue += decibinaryValue[start] * (1 << power);
		}
		return currentValue <= maxPosible;
	}
}
