package dynamicprogramming;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * @see <a href="https://www.hackerrank.com/challenges/fibonacci-modified">Fibonacci Modified</a>
 */
public class FibonacciModified {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		long ti_0 = in.nextLong();
		long ti_1 = in.nextLong();
		int n = in.nextInt();
		in.close();
		BigInteger[] values = new BigInteger[3];
		values[0] = BigInteger.valueOf(ti_0);
		values[1] = BigInteger.valueOf(ti_1);
		for (int i = 2; i < n; i++) {
			values[i % 3] = values[(i - 2) % 3].add(values[(i - 1) % 3].multiply(values[(i - 1) % 3]));
		}
		System.out.println(values[n - 1]);
	}
}
