package recursion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * @see <a href="https://www.hackerrank.com/challenges/password-cracker">Password Cracker</a>
 */
public class PasswordCracker {
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		for (int i = 0; i < T; i++) {
			int N = in.nextInt();
			String[] passwords = new String[N];
			for (int j = 0; j < N; j++) {
				passwords[j] = in.next();
			}
			String loginAttempt = in.next();
			//
			Arrays.sort(passwords, (String a, String b) -> {
				if (a.startsWith(b) || b.startsWith(a)) {
					return b.length() - a.length();
				} else {
					return a.compareTo(b);
				}
			});
			Map<Character, List<char[]>> dictionary = new HashMap<>();
			for (String password : passwords) {
				List<char[]> list;
				if (dictionary.containsKey(password.charAt(0))) {
					list = dictionary.get(password.charAt(0));
				} else {
					list = new ArrayList<>();
					dictionary.put(password.charAt(0), list);
				}
				list.add(password.toCharArray());
			}
			Deque<String> passwordSequence = new LinkedList<>();
			if (isViableAttemp(passwords, loginAttempt)
					&& willAccept(loginAttempt.toCharArray(), 0, dictionary, passwordSequence)) {
				for (int j = 0, k = passwordSequence.size() - 1; j < k; j++) {
					System.out.print(passwordSequence.pollFirst() + " ");
				}
				System.out.println(passwordSequence.pollFirst());
			} else {
				System.out.println("WRONG PASSWORD");
			}
		}
		in.close();
	}

	private static boolean isViableAttemp(String[] passwords, String loginAttempt) {
		Character startsWith = loginAttempt.charAt(0), endsWith = loginAttempt.charAt(loginAttempt.length() - 1);
		boolean satisfiesStartsWith = false, satisfiesEndsWith = false;
		for (String password: passwords) {
			if (password.startsWith(startsWith.toString())) {
				satisfiesStartsWith = true;
			}
			if (password.endsWith(endsWith.toString())) {
				satisfiesEndsWith = true;
			}
		}
		if (satisfiesStartsWith && satisfiesEndsWith) {
			Set<Character> alphabet = new HashSet<>();
			for(Character a:loginAttempt.toCharArray()) {
				alphabet.add(a);
			}
			for (String password: passwords) {
				for(Character c: password.toCharArray()) {
					alphabet.remove(c);
				}
				if(alphabet.isEmpty()) {
					return true;
				}
			}
		}
		return false;
	}

	private static boolean willAccept(char[] loginAttempt, int position, Map<Character, List<char[]>> dictionary,
			Deque<String> passwordSequence) {
		List<char[]> filteredPasswords = dictionary.get(loginAttempt[position]);
		if (filteredPasswords == null) {
			return false;
		}
		for (char[] password : filteredPasswords) {
			if (position + password.length > loginAttempt.length) {
				continue;
			}
			boolean isMatch = true;
			for (int i = 0; i < password.length; i++) {
				if (password[i] != loginAttempt[position + i]) {
					isMatch = false;
					break;
				}
			}
			if (!isMatch) {
				continue;
			}
			if (position + password.length == loginAttempt.length
					|| willAccept(loginAttempt, position + password.length, dictionary, passwordSequence)) {
				passwordSequence.addFirst(String.copyValueOf(password));
				return true;
			}
		}
		return false;
	}
}
