package recursion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * @see <a href="https://www.hackerrank.com/challenges/crossword-puzzle">Crossword Puzzle</a>
 */

public class CrosswordPuzzle {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		char[][] puzzle = new char[10][];
		for (int i = 0; i < 10; i++) {
			puzzle[i] = in.next().toCharArray();
		}
		String[] words = in.next().split(";");
		in.close();
		//
		List<PuzzleEntry> puzzleEntries = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (puzzle[i][j] == '-' || puzzle[i][j] == '*') {
					puzzle[i][j] = '*';
					if (i + 1 < 10 && puzzle[i + 1][j] == '-') {
						PuzzleEntry entry = new PuzzleEntry(i, j);
						entry.isHorizontal = false;
						for (; i + entry.size < 10 && puzzle[i + entry.size][j] != '+'; entry.size++) {
							puzzle[i + entry.size][j] = '*';
						}
						puzzleEntries.add(entry);
					}
					if (j + 1 < 10 && puzzle[i][j + 1] == '-') {
						PuzzleEntry entry = new PuzzleEntry(i, j);
						entry.isHorizontal = true;
						for (; j + entry.size < 10 && puzzle[i][j + entry.size] != '+'; entry.size++) {
							puzzle[i][j + entry.size] = '*';
						}
						puzzleEntries.add(entry);
					}
				}
			}
		}
		List<DictionaryEntry> dictionary = new ArrayList<>();
		for (String word : words) {
			dictionary.add(new DictionaryEntry(word));
		}
		//
		char[][] solution = findSolution(puzzle, puzzleEntries, 0, dictionary);
		//
		for (char[] row : solution != null ? solution : puzzle) {
			System.out.println(row);
		}
	}

	private static char[][] findSolution(char[][] puzzle, List<PuzzleEntry> puzzleEntries, int currentEntry,
			List<DictionaryEntry> dictionary) {
		for (DictionaryEntry dictionaryEntry : dictionary) {
			if (canFit(puzzle, dictionaryEntry, puzzleEntries.get(currentEntry))) {
				dictionaryEntry.inUse = true;
				char[][] tentativeSolution = addWord(puzzle, puzzleEntries.get(currentEntry), dictionaryEntry);
				if (puzzleEntries.size() == currentEntry + 1) {
					return tentativeSolution;
				} else {
					char[][] solution = findSolution(tentativeSolution, puzzleEntries, currentEntry + 1, dictionary);
					if(solution != null) {
						return solution;
					}
				}
				dictionaryEntry.inUse = false;
			}
		}
		return null;
	}

	private static boolean canFit(char[][] puzzle, DictionaryEntry dictionaryEntry, PuzzleEntry puzzleEntry) {
		if (dictionaryEntry.inUse || dictionaryEntry.word.length() != puzzleEntry.size) {
			return false;
		}
		char[] word = dictionaryEntry.word.toCharArray();
		if (!puzzleEntry.isHorizontal) {
			for (int i = 0; i < word.length; i++) {
				if (puzzle[i + puzzleEntry.x][puzzleEntry.y] != '*'
						&& puzzle[i + puzzleEntry.x][puzzleEntry.y] != word[i]) {
					return false;
				}
			}
		} else {
			for (int i = 0; i < word.length; i++) {
				if (puzzle[puzzleEntry.x][i + puzzleEntry.y] != '*'
						&& puzzle[puzzleEntry.x][i + puzzleEntry.y] != word[i]) {
					return false;
				}
			}
		}
		return true;
	}

	private static char[][] addWord(char[][] puzzle, PuzzleEntry puzzleEntry, DictionaryEntry dictionaryEntry) {
		char[][] updatedPuzzle = new char[10][];
		for (int i = 0; i < 10; i++) {
			updatedPuzzle[i] = Arrays.copyOf(puzzle[i], puzzle[i].length);
		}
		char[] word = dictionaryEntry.word.toCharArray();
		if (!puzzleEntry.isHorizontal) {
			for (int i = 0; i < puzzleEntry.size; i++) {
				updatedPuzzle[i + puzzleEntry.x][puzzleEntry.y] = word[i];
			}
		} else {
			for (int i = 0; i < puzzleEntry.size; i++) {
				updatedPuzzle[puzzleEntry.x][i + puzzleEntry.y] = word[i];
			}
		}
		return updatedPuzzle;
	}

	static class PuzzleEntry {
		public int x;
		public int y;
		public boolean isHorizontal;
		public int size;

		public PuzzleEntry(int x, int y) {
			this.x = x;
			this.y = y;
			this.size = 1;
		}
	}

	static class DictionaryEntry {
		public String word;
		boolean inUse;

		public DictionaryEntry(String word) {
			this.word = word;
			this.inUse = false;
		}
	}
}
