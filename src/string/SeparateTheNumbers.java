package string;

import java.util.Scanner;

/**
 * @see <a href="https://www.hackerrank.com/challenges/separate-the-numbers">Separate the Numbers</a>
 */
public class SeparateTheNumbers {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int q = in.nextInt();
		for (int a = 0; a < q; a++) {
			char[] n = in.next().toCharArray();
			int maxSize = n.length / 2;
			int length = 1;
			if (length <= maxSize) {
				do {
					if (isValidSequence(n, 0, length)) {
						break;
					}
					length++;
				} while (length <= maxSize);
			}
			if (length > maxSize) {
				System.out.println("NO");
			} else {
				StringBuilder buf = new StringBuilder("YES ");
				for (int i = 0; i < length; i++) {
					buf.append(n[i]);
				}
				System.out.println(buf.toString());
			}
		}
		in.close();
	}

	private static boolean isValidSequence(char[] n, int start, int length) {
		if (n[start + length] == '0') {
			return false;
		}
		char[] next = Long.toString(getValue(n, start, length) + 1).toCharArray();
		if (!matchesRegion(n, start + length, next)) {
			return false;
		} else if (n.length == next.length + start + length) {
			return true;
		} else {
			return isValidSequence(n, start + length, next.length);
		}
	}

	private static boolean matchesRegion(char[] n, int start, char[] next) {
		if (n.length < start + next.length) {
			return false;
		}
		for (int i = 0; i < next.length; i++) {
			if (n[start + i] != next[i]) {
				return false;
			}
		}
		return true;
	}

	private static long getValue(char[] n, int start, int length) {
		long value = 0, multiplier = 1;
		do {
			length--;
			value += ((n[start + length] - '0') * multiplier);
			multiplier *= 10;
		} while (length > 0);
		return value;
	}

}
